#!/bin/bash

# Run this file to make a basic project to test phpstan
mkdir newproject
cp ./{*.json,*.php} newproject/
cd newproject
wget https://getcomposer.org/installer
php installer
php composer.phar require --dev phpstan/phpstan
echo "Running on PHPStan Level 7"
php vendor/bin/phpstan analyse test.php --level 7